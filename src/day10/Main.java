package day10;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {
				System.out.println("Masukan Case Berapa: ");
				int number = input.nextInt();
				input.nextLine();
				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Case is not Available");
					break;
				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Lanjutkan?");
			answer = input.next();
		}
	}

	private static void soal1() {
		System.out.println("Big Sorting");
		System.out.println("Masukan Panjang Array: ");
		int inputan = input.nextInt();
		System.out.println("Masukan Array: ");
		String[] ArrayString = new String[inputan];
		for (int i = 0; i < ArrayString.length; i++) {
			ArrayString[i] = input.next();
		}

		BigDecimal[] bigArrays = new BigDecimal[ArrayString.length];

		for (int i = 0; i < ArrayString.length; i++) {
			bigArrays[i] = new BigDecimal(ArrayString[i]);
		}
		BigDecimal temp = new BigDecimal("0");
		for (int i = 0; i < bigArrays.length; i++) {
			for (int j = 0; j < bigArrays.length; j++) {
				if (bigArrays[i].compareTo(bigArrays[j]) == 1 || bigArrays[i].compareTo(bigArrays[j]) == 0) {
					temp = bigArrays[i];
					bigArrays[i] = bigArrays[j];
					bigArrays[j] = temp;
				}
			}
		}
		for (int i = 0; i < bigArrays.length; i++) {
			System.out.println(bigArrays[i] + " ");
		}

//			
	}

	private static void soal2() {
		System.out.println("Insertion Sort");
		System.out.println("Masukan Panjang Array: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Data: ");
		String data = input.nextLine();

		String[] baris = data.split(" ");

		if (baris.length != jumlah) {
			System.out.println("Maaf, Data Yang anda masukan tidak sesuai");
			return;
		}
		int[] barisUrut = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
			barisUrut[i] = Integer.parseInt(baris[i]);
		}

		int temp = 0;
		for (int i = jumlah - 1; i > 0; i--) {
			for (int j = jumlah - 1; j >= 0; j--) {
				temp = barisUrut[i];
				barisUrut[i] = barisUrut[j];
				barisUrut[j] = temp;
				break;
			}
		}
		for (int k = 0; k < jumlah; k++) {
			if (barisUrut[k] == 3 && k > 1) {
				if (k == 0) {
					System.out.println(barisUrut[k - 1]);
				} else {
					System.out.println(" " + barisUrut[k - 1]);
				}
			} else {
				if (k == 0) {
					System.out.println(barisUrut[k]);
				} else {
					System.out.println(" " + barisUrut[k]);
				}
			}
		}
		System.out.println();

	}

	private static void soal3() {
		System.out.println("Correctness and Loop Invariant");
		System.out.println("Masukan Panjang Array: ");
		int panjang = input.nextInt();
		System.out.println("Masukan Inputan: ");
		String inputan = input.nextLine();
		String[] ArrInput = inputan.split(" ");

		if (panjang != ArrInput.length) {
			System.out.println("Salah Memasukan Panjang Array");
			return;
		}

		int[] ArrInputInt = new int[panjang];
		for (int i = 0; i < panjang; i++) {
			ArrInputInt[i] = Integer.parseInt(ArrInput[i]);
		}

		int temp = 0; // sorting
		for (int i = 0; i < panjang; i++) {
			for (int j = 0; j < panjang; j++) {
				if (ArrInputInt[i] <= ArrInputInt[j]) {
					temp = ArrInputInt[i];
					ArrInputInt[i] = ArrInputInt[j];
					ArrInputInt[j] = temp;
				}
			}
		}

		for (int i = 0; i < panjang; i++) {
			System.out.println(ArrInputInt[i] + " ");
		}
	}

	private static void soal4() {
		System.out.println("Running Time of Algorithms");
		System.out.println("Masukan Panjang Array: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Data: ");
		String data = input.nextLine();

		String[] baris = data.split(" ");

		if (baris.length != jumlah) {
			System.out.println("Maaf Data yang dimasukan tidak benar");
			return;
		}
		int[] barisUrut = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
			barisUrut[i] = Integer.parseInt(baris[i]);
		}

		int temp = 0;
		int step = 0;
		for (int i = 0; i < jumlah; i++) {
			for (int j = i; j < jumlah; j++) {
				if (barisUrut[i] > barisUrut[j]) {
					temp = barisUrut[j];
					barisUrut[j] = barisUrut[i];
					barisUrut[i] = temp;
					step++;
				}
			}
		}
		System.out.println(step);
	}

	private static void soal5() {
		System.out.println("Counting Sort 1");
		System.out.println("Masukan Jumlah Datanya: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Data: ");
		String data = input.nextLine();

		String[] baris = data.split(" ");

		if (baris.length != jumlah) {
			System.out.println("Maaf Data yang dimasukan tidak sesuai");
			return;
		}
		int[] jmlh = new int[jumlah];
		int[] larik = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
			larik[i] = Integer.parseInt(baris[i]);
			jmlh[larik[i]] += 1;
		}

		for (int i = 0; i < jumlah; i++) {
			System.out.println(jmlh[i] + " ");
		}

	}

	private static void soal6() {
		System.out.println("Counting Sort 2");
		System.out.println("Masukan Panjang Array: ");

	}

	private static void soal7() {
		System.out.println("Find The Median");
		System.out.println("Masukan Panjang Array: ");
		int a = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Angkanya: ");
		int n = input.nextInt();
		int[] inputan = new int[n];

		if (a != inputan.length) {
			System.out.println("Panjang Tidak Sesuai");
		}
		int temp = 0;
		for (int i = 0; i < inputan.length; i++) {
			inputan[i] = input.nextInt();
		}
		if (inputan.length % 2 == 0) {
			temp = inputan.length / 2;

		} else {
			temp = inputan[inputan.length / 2];
		}

		System.out.println("Mediannya Adalah " + temp);

	}

	private static void soal8() {
		System.out.println("Sorting The Alfabet");
		System.out.println("Masukan Sampel: ");
		String data = input.nextLine();

		char[] dataChar = data.toCharArray();

		char temp = ' ';
		for (int i = 0; i < dataChar.length; i++) {
			for (int j = 0; j < dataChar.length; j++) {

				if (dataChar[i] < dataChar[j]) {
					temp = dataChar[i];
					dataChar[i] = dataChar[j];
					dataChar[j] = temp;
				}
			}
		}
		for(int i=0; i<dataChar.length; i++) {
			System.out.println(dataChar[i]);
		}
	}

}
