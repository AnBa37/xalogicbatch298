package day1;

import java.util.Scanner;

public class Percabangan {
	
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		//contoh dengan nilai
		
		System.out.println("Masukin Nama Siswa :");
		String nama = input.next();
		System.out.println("Masukan Nilai Siswa: ");
		int nilai = input.nextInt();
		System.out.println("Masukan nilai SoftSkill Siswa: ");
		int nilaiSoftSkill = input.nextInt(); //untuk input nilai softskill
		
		input.nextLine();//Untuk Mengubah input dari int ke String
		System.out.println("Masukan Batch :");
		String batch = input.nextLine(); //untuk input string  yang bisa menggunakan spasi
		
		String grade = "";
		String kelulusan = "";
		//nilai >= 90 = A
		//nilai >= 85 = B+
		//nilai >= 80 = B, Kelulusan = LULUS
		//nilai >= 75 = B-, Kelulusan = LULUS
		//nilai >= 70 = C , Kelulusan = REMEDIAL
		if(nilai>=90 && nilaiSoftSkill >=3) {
			System.out.println( nama + " " + batch + grade + kelulusan +" Selamat Anda Lulus dengan Nilai A");
		} else if(nilai >=85 && nilaiSoftSkill >=3){
			System.out.println(nama + " " + batch + grade + kelulusan + " Selamat Anda Lulus dengan nilai B+");
		} else if(nilai >=80 && nilaiSoftSkill >=3) {
			System.out.println(nama + " " + batch + grade + kelulusan + " Selamat anda Lulus dengan Nilai B");
		} else if(nilai >=75 && nilaiSoftSkill >=3) {
			System.out.println(nama + " " + batch + grade + kelulusan + " Selamat Anda Lulus dengan Nilai B-");
		} else if(nilai >=70 && nilaiSoftSkill >=3) {
			System.out.println(nama + " " + batch + grade + kelulusan + " Maaf  Anda Remedial dengan nilai C");
		} else {
			System.out.println("Anda Tidak Lulus");
		}
	} 

}
