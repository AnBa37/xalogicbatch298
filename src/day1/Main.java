package day1;

public class Main {
	
	public static void main(String args[]) {
		int jef=12;
		char i = 'i';
		float phi = 3.14f;
		double ipk = 3.7;
		String ganteng = "Achengk";
		boolean benar = true;
		
		System.out.println(jef);
		System.out.println(i);
		System.out.println(phi);
		System.out.println(ipk);
		System.out.println(ganteng);
		System.out.println(benar);
		
		String nama, alamat;
		int usia;
		double tinggi;
		
		nama = " Anggit Bagaskara";
		alamat = " Haji Nawi Raya";
		usia = 23;
		tinggi = 171;
		
		System.out.println("Nama  :" + nama);
		System.out.println("Alamat : " + alamat);
		System.out.println("Usia " + usia + " Tahun");
		System.out.println("Tinggi " + tinggi + " Cm");
		
		int nilaiSatu = 5;
		int nilaiDua = 3;
		System.out.println("=======================");
		System.out.println("Jumlah nilaiSatu dan nilaiDua :" + (nilaiSatu + nilaiDua));
		//logic
		nilaiSatu = nilaiSatu + nilaiDua;
		nilaiDua = nilaiSatu - nilaiDua;
		nilaiSatu = nilaiSatu - nilaiDua;
		System.out.println("Nilai Satu : " + nilaiSatu);
		System.out.println("Nilai Dua : " + nilaiDua);
	}
	
}