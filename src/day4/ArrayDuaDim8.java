package day4;

import java.util.Scanner;

public class ArrayDuaDim8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
	
		System.out.println("Masukan Nilai N: ");
		int n = input.nextInt();
		int temp = 0;
		
		int[][] soalDuaDim8 = new int[3][n];
		
		System.out.println();
		
		for(int i=0; i<3; i++) {
			if(i % 3 == 0) {
				for(int j=0; j < n; j++) {
					soalDuaDim8[i][j] = temp;
					System.out.println(soalDuaDim8[i][j] + " ");
					temp++;
				}
				System.out.println();
			} else if(i%3 == 1) {
				temp =0;
				
				for(int j =0; j<n; j++) {
					soalDuaDim8[i][j] = temp;
					System.out.println(soalDuaDim8[i][j] +" ");
					temp = temp +2;
				}
				System.out.println();
			} else if(i%3==2) {
				for(int j=0; j<n; j++) {
					soalDuaDim8[i][j] = soalDuaDim8[0][j] + soalDuaDim8[1][j];
					System.out.println(soalDuaDim8[i][j] + " ");
					temp++;
				}
			}
		}
	}

}
