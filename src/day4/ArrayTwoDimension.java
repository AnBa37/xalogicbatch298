package day4;

import java.util.Scanner;

public class ArrayTwoDimension {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//kotak pertama baris kotak kedua kolom
		System.out.println("Masukan Baris : ");
		int baris = input.nextInt();
		System.out.println("Masukan Kolom : ");
		int kolom = input.nextInt();
		int n = 1;
		int[][] arrayAngkaDuaDimen = new int[baris][kolom];
		for(int i= 0; i< baris; i++) {
			for(int j= 0; j < kolom; j++) {
				arrayAngkaDuaDimen[i][j] = n;
				System.out.print(arrayAngkaDuaDimen[i][j] + "     ");
				n++;
			}
			System.out.println();
		}
		

	}

}
