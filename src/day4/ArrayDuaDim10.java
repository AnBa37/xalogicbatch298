package day4;

import java.util.Scanner;

public class ArrayDuaDim10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Nilai N: ");
		int n = input.nextInt();
		int temp = 0;
		
		int[] soalSepuluh = new int[n];
		
		for(int i = 0; i < n; i++) {
			if(i%4 == 3) {
				temp = temp *3;
				System.out.println("XXX ");
			} else {
				temp = temp * 3;
				soalSepuluh[i] = temp;
				System.out.println(soalSepuluh[i] + " ");
			}
		}
	}

}
