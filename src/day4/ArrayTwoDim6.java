package day4;

import java.util.Scanner;

public class ArrayTwoDim6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Nilai N: ");
		int n = input.nextInt();
		
		int temp= 0;
		int[][] soalDuaDim6 = new int[3][n];
		
		System.out.println();
		
		for (int i = 0; i <3; i++) {
			if(i%3 == 0) {
				for(int j=0; j<n;j++) {
					soalDuaDim6[i][j] = i;
					System.out.println(soalDuaDim6[i][j]+ " ");
					temp++;
				}
				System.out.println();
			} else if(i %3 == 1) {
				temp = 1;
				for(int j = 0; j < n; j++) {
					soalDuaDim6[i][j] = i;
					System.out.println(soalDuaDim6[i][j] + " ");
					temp = temp * n;
				}
				System.out.println();
			} else if(i%3==2) {
				for(int j=0; j < n; j++) {
					soalDuaDim6[i][j] = soalDuaDim6[i-2][j] + soalDuaDim6[i-1][j];
					System.out.println(soalDuaDim6[i][j]+" ");
					temp++;
				}
			}
		}
	}

}
