package day4;

import java.util.Scanner;

public class ArrayTwoDim4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Nilai N1: ");
		int kolom = input.nextInt();
		System.out.println("Masukan Nilai N2: ");
		int baris = input.nextInt();
		
		int[][] ArrayDuaDimen4 = new int[baris-1][kolom];
		int temp = 0;
		for (int i= 0; i < baris - 1 ; i++) {
			if(i%2 == 1) {
				for (int j = 0; j < kolom; j++) {
					ArrayDuaDimen4[i][j]= temp;
					System.out.print(ArrayDuaDimen4[i][j] + " ");
					temp++;
				}
				System.out.println();
			} else if(i%2==1) {
				temp = 1;
				int temp1 = 5;
				for(int j = 0; j < kolom; j++) {
					if(j%2==0) {
						ArrayDuaDimen4[i][j] = temp;
						System.out.print(ArrayDuaDimen4[i][j] + " ");
						temp=temp+5;
					} else {
						ArrayDuaDimen4[i][j]= temp1;
						System.out.print(ArrayDuaDimen4[i][j] + " ");
						temp1= temp1+5;
					}
				}
				System.out.println();
			}
		}
	}

}
