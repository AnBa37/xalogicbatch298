package day4;

import java.util.Scanner;

public class ArrayTwoDimNo3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Masukin N1 :");
		int kolom = input.nextInt();
		System.out.println("Masukin N2: ");
		int baris = input.nextInt();
		
		int[][] ArrayDuaDimen3 = new int[baris - 1][kolom];
		int temp = 0;
		for (int i = 0; i < baris - 1; i++) {
			if(i%2 == 0) {
				for(int j = 0; j < kolom; j++) {
					ArrayDuaDimen3[i][j] = temp;
					System.out.print(ArrayDuaDimen3[i][j] + " ");
					temp++;
				}
				System.out.println();
			} else if(i%2==1) {
				temp = 3;
				for(int j = 0; j < kolom; j++) {
					if(j%6==3 || j==4 || j==5) {
						ArrayDuaDimen3[i][j] = temp;
						System.out.print(ArrayDuaDimen3[i][j] + " ");
						temp = temp*1/2;
					} else {
						ArrayDuaDimen3[i][j] = temp;
						System.out.print(ArrayDuaDimen3[i][j] + " ");
						temp = temp*2;
					}
				}
				
			}
		}
	}

}
