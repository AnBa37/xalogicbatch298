package day4;

import java.util.Scanner;

public class ArrayDuaDim9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Nilai N1:");
		int n1 = input.nextInt();
		System.out.println("Masukan N2: ");
		int n2 = input.nextInt();
		
		int temp = 0;
		int[][] soalDuaDim9 = new int[n2][n1];
		
		System.out.println();
		
		for (int i =0; i < n2; i++) {
			if(i%3 ==0) {
				for(int j =0; j<n1; j++) {
					soalDuaDim9[i][j] = temp;
					System.out.println(soalDuaDim9[i][j]+" ");
					temp++;
				}
				System.out.println();
			} else if(i%3 == 1) {
				temp = 0;
				
				for(int j = 0; j < n1; j++) {
					soalDuaDim9[i][j] = temp;
					System.out.println(soalDuaDim9[i][j] + " ");
					temp = temp +n2;
				}
				System.out.println();
			} else if(i%3==2) {
				temp = temp - n2;
				
				for(int j = 0; j < n1; j++) {
					soalDuaDim9[i][j] = temp;
					System.out.println(soalDuaDim9[i][j] + " ");
					temp = temp - n2;
				}
				System.out.println();
			}
		}
	}

}
