package day4;

import java.util.Scanner;

public class ArrayTwoDimNo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan n1: ");
		int kolom = input.nextInt();
		System.out.println("Masukan n2: ");
		int baris = input.nextInt();

		int[][] ArrayDuaDimen2 = new int[baris - 1][kolom];
		int temp = 0;
		for (int i = 0; i < baris - 1; i++) {
			if (i % 2 == 0) {
				for (int j = 0; j < kolom; j++) {
					ArrayDuaDimen2[i][j] = temp;
					System.out.print(ArrayDuaDimen2[i][j] + " ");
					temp++;
				}
				System.out.println();

			} else if (i % 2 == 1) {
				temp = 1;
				for (int j = 0; j < kolom; j++) {
					if (j % 3 == 2) {
						ArrayDuaDimen2[i][j] = -temp;
						System.out.print(ArrayDuaDimen2[i][j] + " ");
						temp = temp * 3;

					} else {
						ArrayDuaDimen2[i][j] = temp;
						System.out.print(ArrayDuaDimen2[i][j] + " ");
						temp = temp*3;
					}
				}
			}

		}
	}
}