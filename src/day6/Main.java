package day6;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
				default:
					System.out.println("Case is not Available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("Disini Nomor 1");

		System.out.println("Masukan nilai Fibonachi : ");
		int fibonachi = input.nextInt();
		input.nextLine();
		int[] arrFibo = new int[fibonachi];
		String temp = "";
		for (int i = 0; i < fibonachi; i++) {
			if (i > 1) {
				arrFibo[i] = arrFibo[i - 1] + arrFibo[i - 2]; // nilai Fibonachi
			} else {
				arrFibo[i] = 1;
			}
			temp += arrFibo[i];

			if (i < fibonachi - 1) { // untuk koma
				temp += ",";
			}
		}
		System.out.println(temp);
	}

	private static void soal2() {
		System.out.println("Disini Nomor 2");

		System.out.println("Masukan Nilai Fibonachi : ");
		int fibonachi = input.nextInt();
		int[] arrFibo = new int[fibonachi];
		String temp = "";
		for (int i = 0; i < fibonachi; i++) {
			if (i > 2) {
				arrFibo[i] = arrFibo[i - 3] + arrFibo[i - 1] + arrFibo[i - 2];
			} else {
				arrFibo[i] = 1;
			}
			temp += arrFibo[i];

			if (i < fibonachi - 1) {
				temp += ",";
			}
		}
		System.out.println(temp);
	}

	private static void soal3() {
		System.out.println("Disini No. 3");

		System.out.println("Masukan Angka N: ");
		int prima = input.nextInt();
		// int[] bilprima = new int[prima];
		for (int i = 0; i < prima; i++) {
			int bil = 0;
			for (int j = 1; j <= i; j++) {
				if (i % j == 0) {
					bil = bil + 1;
				}
			}
			if (bil == 2) {
				System.out.print(i + " ");
			}
		}

	}

	private static void soal4() {
		System.out.println("Disini No. 4");
		System.out.println();
		System.out.println("Masukan Jam: ");
		String jam = input.next();

		char[] cJam = jam.toCharArray();

		String JMDetik = jam.substring(0, 8);
		String JamS = JMDetik.substring(0, 2);
		String MDe = JMDetik.substring(2, 8);
		String AmPm = jam.substring(8, 10);

		String temp = " ";

		if (AmPm.equals("PM")) {
			temp += (12 + Integer.parseInt(JamS)) + MDe;
		} else {
			temp = JMDetik;
		}
		System.out.println(temp);

	}

	private static void soal5() {
		System.out.println("Disini No. 5");
		System.out.println();
		System.out.println("Masukan Inputan");
		int n = input.nextInt();
		int prima = 0;
		int hasil = 0;
		while (n != 1) {
			hasil = n;
			if (hasil % prima == 0) {
				hasil = n / prima;
				System.out.println("" + n + "/" + prima + "=" + hasil);
				// n = hasil;
			} else {
				prima++;
			}
		}

	}

	private static void soal6() {
		System.out.println("Disini No. 6");
		System.out.println("");

		System.out.println("Masukan Rute Tempuh: ");
		String rute = input.nextLine();

		String[] rute1 = rute.split(" ");
		int[] ruteInt = new int[rute1.length]; // dijadiin int
		double hasil = 0;
		double bensin = 2.5; // bensin per 1 liter
		double[] jarak2 = { 2, 0.5, 1.5, 0.3 }; // jarak yang sudah di konversi ke KM

		// System.out.println(jarak2[]);
		
		for (int i = 0; i < rute1.length; i++) {
			hasil = hasil + jarak2[Integer.parseInt(rute1[i]) - 1];
			if (i != rute1.length - 1  ) {
				System.out.print(jarak2[Integer.parseInt(rute1[i]) - 1] + " + ");
			} else {
				System.out.print(jarak2[Integer.parseInt(rute1[i]) - 1] + " = " + hasil + " KM ");
			} //else {
				//System.out.println("Maaf Rute Tidak Ada");
			//}
			//if(Integer.parseInt(rute1[i])> rute1.length) {
				//System.out.println("Maaf Rute Tidak ada");
			//}
		}
		System.out.println();
		bensin = hasil / bensin;
		bensin = Math.ceil(bensin);
		String tempString = "Jadi Bensin Yang Di Konsumsi = " + bensin + " Liter";
		System.out.println(tempString.replaceAll(".0", ""));
		
		// System.out.println(bensin);

	}

	private static void soal7() {
		System.out.println("Disini No.7 ");
		System.out.println("============");
		System.out.println("Masukan Pesan SOS: ");
		String sos = input.nextLine();

		char[] sos1Char = sos.toLowerCase().toCharArray();
		int temp = 0;
		for (int i = 0; i < sos1Char.length; i++) {
			if (i % 3 == 0 && sos1Char[i] == 's') {
				temp++;
			} else if (i % 3 == 1 && sos1Char[i] == 'o') {
				continue;
			} else if (i % 3 == 2 && sos1Char[i] == 's') {
				temp++;
			} else {
				temp++;
			}
		}
//		for(int i= 0; i< sos.length; i+=3) {
//			if
//		}
		System.out.println(temp);
	}

}
