package day5;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				default:
					System.out.println("Case is not Available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("Disini Soal 1");
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Uang:"); // Masukin Uang int
		int Uang = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Harga Baju: ");
		String hargaBaju = input.next(); // input String
		System.out.println("Masukan Harga Celana: ");
		String hargaCelana = input.next(); // input String

		String[] hargaBaju1 = hargaBaju.split(","); // split harga baju ke ,
		int[] hargaBaju2 = new int[hargaBaju1.length]; // menampilkan array harga baju
		for (int i = 0; i < hargaBaju2.length; i++) {
			hargaBaju2[i] = Integer.parseInt(hargaBaju1[i]); // convert String ke integer untuk harga Baju
		}

		String[] hargaCelana1 = hargaCelana.split(","); // split harga celana ke ,
		int[] hargaCelana2 = new int[hargaCelana1.length]; // menampilkan array harga celana
		for (int i = 0; i < hargaCelana2.length; i++) {
			hargaCelana2[i] = Integer.parseInt(hargaCelana1[i]);
		}
		int total = 0;
		int maks = 0;

		for (int baju = 0; baju < hargaBaju2.length; baju++) {
			for (int celana = 0; celana < hargaCelana2.length; celana++) {
				total = hargaCelana2[celana] + hargaBaju2[baju];
				if (maks < total && total <= Uang) {
					maks = total;
					// if (maks < Uang) {

					// }
				}
			}
		}
		System.out.println(maks);
	}
	// int[] baju = new int[5];
	// input.nextLine();
	// System.out.println("Masukan Harga baju 1: ");
	// baju[0] = input.nextInt();
	// System.out.println("Masukan Harga baju 2: ");
	// baju[1] = input.nextInt();
	// System.out.println("Masukan Harga baju 3: ");
	// baju[2] = input.nextInt();
	// System.out.println("Masukan Harga baju 4: ");
	// baju[3] = input.nextInt();

	// int[] celana = new int[5];
	// System.out.println("Masukan Harga Celana 1: ");
	// celana[0] = input.nextInt();
	// System.out.println("Masukan Harga Celana 2: ");
	// celana[1] = input.nextInt();
	// System.out.println("Masukan Harga Celana 3: ");
	// celana[2] = input.nextInt();
	// System.out.println("Masukan Harga Celana 4: ");
	// celana[3] = input.nextInt();

	// System.out.print(baju[0] + celana[0]);
	// System.out.println(baju[1] + celana[1]);
	// System.out.println(baju[2] + celana[2]);
	// System.out.println(baju[3] + celana[3]);
	// System.out.println(baju[4] + celana[4]);
	// int bc1 = baju[0] + celana[0];
	// int bc2 = baju[1] + celana[1];
	// int bc3 = baju[2] + celana[2];
	// int bc4 = baju[3] + celana[3];
	// String[] arrayBaju = {35,40,50,20};
	// String[] arrayCelana = {40,30,45,10};
	// int b1 = 35, b2 = 40, b3 = 50, b4 = 20;
	// int c1 = 40, c2 = 30, c3 = 45, c4 = 10;
	// int bc1 = b1 + c1;
	// int bc2 = b2 + c2;
	// int bc3 = b3 + c3;
	// int bc4 = b4 + c4;

	// if (Uang >= 75 && Uang <= 94) {
	// System.out.println("======");
	// System.out.println(bc1);
	// } else if (Uang >= 95) {
	// System.out.println("======");
	// System.out.println(bc3);
	// } else if (Uang >= 70) {
	// System.out.println("=======");
	// System.out.println(bc2);
	// } else if (Uang >= 30) {
	// System.out.println("=======");
	// System.out.println(bc4);
	// } else {
	// System.out.println("Uang Anda Tidak memenuhi");
	// }
	// }

	private static void soal2() {
		System.out.println("Disini Soal 2");

		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Inputan: ");
		String inputan = input.next();
		input.nextLine();
		System.out.println("Masukan n: ");
		int geser = input.nextInt();

		String[] inputan1 = inputan.split(",");
		int[] inputan2 = new int[inputan1.length];
		int[] geser1 = new int[inputan1.length];

		for (int i = 0; i < inputan1.length; i++) {
			inputan2[i] = Integer.parseInt(inputan1[i]);
		}

		// hasil input
		for (int i = 0; i < geser1.length; i++) {
			int temp = inputan2[0];
			String result = "";
			for (int j = 0; j < geser1.length; j++) {
				if (j == inputan2.length - 1) {
					inputan2[j] = temp;
					result = result + temp;
				} else {
					inputan2[j] = inputan2[j+1];
					result = result + inputan2[j] + ",";
				}

			}
			System.out.println((i+1) + ": " +result);
			System.out.println();
		}
	}

	private static void soal3() {
		System.out.println("Disini Soal 3");
		System.out.println("Masukan Jumlah Puntung: "); // Inputin Puntungnya
		int n = input.nextInt();
		int puntung = n; //variabel puntung
		int batang = puntung / 8; //1 batang per 8 puntung
		int sisa = puntung % batang; //variabel sisa rokok
		int penghasilan = 500;
		int total = penghasilan * batang;
		System.out.println("===================");
		System.out.println("Batang Yang Didapatkan :" + batang);
		System.out.println("Penghasilan Yang Didapatkan: Rp." + total);
		System.out.println("Sisa Puntung : " + sisa);

	}

	private static void soal4() {
		System.out.println("Disini Soal 4");
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Jumlah Menu: ");
		int menu = input.nextInt();
		System.out.println("Masukan indeks Alergi: ");
		int alergi = input.nextInt();

		input.nextLine();
		System.out.println("Masukan List Harga Menu: ");
		String harga = input.next();
		input.nextLine();
		System.out.println("Masukan Jumlah Uang: ");
		int uang = input.nextInt();

		String[] harga1 = harga.split(",");
		int[] harga2 = new int[menu];

		for (int i = 0; i < harga1.length; i++) {
			harga2[i] = Integer.parseInt(harga1[i]);
		}
		int total = (harga2[0] + harga2[1] + harga2[2] + harga2[3] - harga2[alergi]) / 2;
		int sisa = uang - total;
		int kurang = total - uang;

		System.out.println("Total Tagihan :" + total);
		if (total > uang) {
			System.out.println(" Maaf Uang Anda Kurang " + kurang);
		} else if (total == uang) {
			System.out.println("Uang Anda Pas :)");
		} else {
			System.out.println("Sisa Uang :" + sisa);
		}
	}

	private static void soal5() {
		System.out.println("Disini Soal 5");
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Kalimat: ");
		String kalimat = input.nextLine();
		char[] kalimat2 = kalimat.toCharArray();
		String konsonan = "";
		String vokal = "";

		for (int i = 0; i < kalimat2.length; i++) { //untuk mengolah okal
			if (kalimat2[i] == 'a' || kalimat2[i] == 'i' || kalimat2[i] == 'u' || kalimat2[i] == 'e'
					|| kalimat2[i] == 'o') {
				vokal += kalimat2[i];
			} else {
				konsonan += kalimat2[i];
			}
		}

		char[] konsonan1 = konsonan.toLowerCase().toCharArray();
		char[] vokal1 = vokal.toLowerCase().toCharArray();

		// untuk vokal
		char temp = 0;
		for (int i = 0; i < vokal1.length; i++) {
			for (int j = 0; j < vokal1.length; j++) {
				if ((int) vokal1[i] <= (int) vokal1[j]) {
					temp = vokal1[i];
					vokal1[i] = vokal1[j];
					vokal1[j] = temp;
				}
			}
		}
		for (int i = 0; i < konsonan1.length; i++) {
			for (int j = 0; j < konsonan1.length; j++) {
				temp = konsonan1[i];
				konsonan1[i] = konsonan1[j];
				konsonan1[j] = temp;
			}
		}
		System.out.println("Vokal :" + String.valueOf(vokal1));
		System.out.println("Konsonan :" + String.valueOf(konsonan1));
	}

	private static void soal6() {
		System.out.println("Disini Soal 6");
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Kalimat: ");
		String kalimat = input.nextLine(); // Inputan Hurufnya
		String[] huruf = kalimat.split(" "); // Misahin kalimatnya

		for (int i = 0; i < huruf.length; i++) {
			char[] kata = huruf[i].toCharArray(); // rubah char ke array
			for (int j = 0; j < kata.length; j++) {
				if (j % 2 == 1) {
					System.out.print("* ");
				} else if (j % 2 == 0) {
					System.out.print(kata[j]);
				}
			}
			if (i == huruf.length - 1) {
			} else {
				System.out.print(" ");
			}
		}
	}
}
	