package day5;

import java.util.Scanner;

public class SpringManipulation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String Manipulation
		String words1 = "Syifa Sabilla Bagaskara"; // ada 3 Array
		String[] tempWords1 = words1.split(" "); // memisahkan kalimat menjadi perkatata (menggunakan split)
		System.out.println(tempWords1[2]); // karena String Berjenis Array maka yang diambil Bagaskara
		System.out.println(tempWords1[tempWords1.length - 1] + " Panjang Array: " + tempWords1.length);// Output Dinamis

		String words2 = "Anggit Bagaskaro";

		char[] charWords1 = tempWords1[2].toCharArray(); // mengubah kata menjadi karakter (menggunakan toCharArray)
		System.out.println(charWords1[charWords1.length - 1]);

		char[] charWords2 = words2.toCharArray();
		System.out.println(charWords2[charWords2.length - 1]);

		String replaceString = words1.replaceAll(" ", ""); // replace all dari a ke i
		System.out.println(replaceString);

		String replaceString1 = words2.replaceAll("a", "i");// replace all dari word 2
		System.out.println(replaceString1);

		char[] charReplaceWords1 = replaceString.toCharArray();// words 1 dipecah jadi char
		System.out.println(charReplaceWords1);
		
		String Bagaskaro = words2.substring(6, 16); //substring untuk output dikeluarkan
		System.out.println(Bagaskaro);
		
		String lowerWords1 = words2.toLowerCase().replaceAll(" ", "");
		System.out.println(lowerWords1);
		String tempBagaskaro = lowerWords1.substring(6,15); //Untuk menggunakan Substring
		System.out.println(tempBagaskaro); //output
		
		char[] tempLowerWords1 = lowerWords1.toCharArray();
		System.out.println(lowerWords1);
		
		String upperWords1 = words1.toUpperCase().replaceAll(" ", "");
		System.out.println(upperWords1);		
		
		//int[] array = new int[] {5,6,7,0,1};
		//Scanner input = new Scanner(System.in);
		//System.out.println("Masukan n: ");
		//int n = input.nextInt();
		//System.out.println("Array Awal");
		//for(int i = 0; i<array.length;i++) {
			//System.out.print(array[i]+" ");
		//}
		//for(int i = 0; i< n; i++) { // array Geser
			//int j, temp;
			//temp = array[array.length-n]; //Nyimpen Elemen terakhir
			
			//for(j = array.length-1; j>0; j--) {
				//array[j] = array[j-1]; //array geser 1x
			//}
			//array[0] = temp;
		//}
		//System.out.println();
		//System.out.println("Setelah Rotasi");
		//for(int i = 0; i< array.length; i++)
			//System.out.print(array[i]+ " ");	
	}

}
