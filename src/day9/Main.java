package day9;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not Available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("Nomor 1");
		System.out.println("Judi Onlen");
		System.out.println();
		System.out.println("Masukan Point: ");
		int point = input.nextInt();

		String pil = "Y";
		while (pil.toUpperCase().equals("Y")) {
			System.out.println("Masukan Taruhan: ");
			int taruhan = input.nextInt();
			System.out.println("Tebak (U/D): ");
			String tebakan = input.next();

			int random = (int) (Math.random() * 9);
			String hasil = "";
			if (random > 5) {
				if (tebakan.equals("U")) {
					point = point + taruhan;
					hasil = "Anda Menang";
				}
				if (tebakan.equals("D")) {
					point = point - taruhan;
					hasil = "Anda Kalah";
				}
			}
			if (random == 5) {
				if (tebakan.equals("D") || tebakan.equals("U")) {
					hasil = "Seri";
				}
			}
			if (random < 5) {
				if (tebakan.equals("D")) {
					point = point + taruhan;
					hasil = "Anda Menang";
				}
				if (tebakan.equals("U")) {
					point = point - taruhan;
					hasil = "Anda Kalah";
				}
			}

		}
	}

	private static void soal2() {
		System.out.println("Nomor 2");
		System.out.println("Keranjang game");
		String[] keranjang = new String[3];
		int totalBuah = 0;
		boolean isLoop = true;
		while (isLoop) {
			System.out.println("Masukan Keranjang 1: ");
			keranjang[0] = input.next();
			System.out.println("Masukan Keranjang 2: ");
			keranjang[1] = input.next();
			System.out.println("Masukan Keranjang 3: ");
			keranjang[2] = input.next();

			boolean isSalah = false;
			totalBuah = 0;
			int[] keranjangInt = new int[keranjang.length];
			for (int i = 0; i < keranjang.length; i++) {
				if (keranjang[i].toLowerCase().trim().equals("kosong") || keranjang[i].isEmpty()) {
					keranjangInt[i] = 0;
					continue;
				}
				char[] charBuah = keranjang[i].toCharArray();
				for (int j = 0; j < charBuah.length; j++) {
					if ((int) (charBuah[j]) >= 48 && (int) (charBuah[j]) <= 57) {
						isSalah = false;
					} else {
						isSalah = true;
						break;
					}
				}
				keranjangInt[i] += Integer.parseInt(keranjang[i]);
			}
			if (isSalah) {
				System.out.println("Anda Salah Memasukan Nilai Keranjang");
				System.out.println("Keranjang 1: Kosong/angka/angka");
				continue;
			}
			int nilaiNol = 0;

			for (int i = 0; i < keranjangInt.length; i++) {
				if (i > 0) {
					totalBuah += keranjangInt[i];
				}
				if (keranjangInt[i] == 0) {
					nilaiNol++;
				}
				if (nilaiNol > 1) {
					System.out.println("Keranjang Kosong lebih dari satu");
					isSalah = true;
					break;
				}
			}
			if (isSalah) {
				continue;
			}
			isLoop = false;
		}
		System.out.println("Keranjang 1 Dibawa Kepasar");
		System.out.println();
		System.out.println("Sisa Buah = " + totalBuah);
	}

	private static void soal3() {
		System.out.println("Ini Nomor 3");
		System.out.println("=============");
		System.out.println("Permainan Siapa Yang Duduk Duluan");
		System.out.println("x = ");
		int x = input.nextInt();
		String hasil = "";
		int temp = 1;

		if (x > 0) {
			for (int i = 1; i <= x; i++) {
				temp *= i;
			}

			System.out.println("Ada sekitar " + temp + " cara untuk duduk ");
		} else {
			System.out.println("Input tidak Sesuai");
		}
	}

	private static void soal4() {
		System.out.println("Soal Nomor 4");
		System.out.println("Pembagian Pakaian Untuk Korban Musibah");

		// Laki Dewasa, Wanita Dewasa, Anak2, Bayi, Laki Dewasa2?
		int[] korban = new int[4];
		System.out.println("Laki-laki Dewasa: ");
		korban[0] = input.nextInt() * 1;
		System.out.println("Wanita Dewasa: ");
		korban[1] = input.nextInt() * 2;
		System.out.println("Anak-Anak: ");
		korban[2] = input.nextInt() * 3;
		System.out.println("Bayi: ");
		korban[3] = input.nextInt() * 5;

		int pakaian = 0;

		for (int i = 0; i < korban.length; i++) {
			pakaian += korban[i];
		}
		if (pakaian % 2 == 1 && pakaian > 10) {
			pakaian -= korban[1]+1;
		}
		System.out.println(pakaian + " Pakaian yang Didapat");

	}

	private static void soal5() {
		System.out.println("Soal No 5.");
		System.out.println("Masak-Masak");
		System.out.println("Jumlah Kue Pukis Yang ingin Dibuat: ");
		int jumlahPukis = input.nextInt();

		int tepung_terigu = 0;
		int gula_pasir = 0;
		int susu_murni = 0;
		int putih_telur = 0;
		System.out.println("Masukan Jumlah Tepung Terigu: ");
		tepung_terigu = input.nextInt();
		System.out.println("Masukan Gula Pasir: ");
		gula_pasir = input.nextInt();
		System.out.println("Masukan Susu Murni: ");
		susu_murni = input.nextInt();
		System.out.println("Masukan Putih Telur: ");
		putih_telur = input.nextInt();
		
		float totalTT = tepung_terigu / jumlahPukis;
		float totalGP = gula_pasir / jumlahPukis;
		float totalSM = susu_murni / jumlahPukis;
		float totalPT = putih_telur / jumlahPukis;
		
		System.out.println("- Tepung Terigu : " + Math.ceil(totalTT));
		System.out.println("- Gula Pasir : " +Math.ceil(totalGP));
		System.out.println("- Susu Murni : " +Math.ceil(totalSM));
		System.out.println("- Putih Telur: " +Math.ceil(totalPT));
//		// bahannya
//		String[] bahan = { "Tepung Terigu", "Gula Pasir", "Susu Murni", "Putih Telur" };
//		int[] jmlhbahan = new int[bahan.length];
//
//		for (int i = 0; i < jmlhbahan.length; i++) {
//			System.out.println("Masukan Jumlah " + bahan[i] + " gram");
//			jmlhbahan[i] = input.nextInt() / jumlahPukis;
//		}
//
//		System.out.println("Jumlah Bahan Yang Diperlukan untuk Membuat 1 buah pukis adalah ");
//		for (int i = 0; i < bahan.length; i++) {
//			System.out.println("- " + bahan[i] + " : " + jmlhbahan[i] + " gram");
//		}

	}

	private static void soal6() throws Exception {
		System.out.println("Nomor 6");
		System.out.println("Parkir");
		System.out.println("Input tgl dan waktu masuk gedung: ");
		String tglMasuk = input.nextLine();
		Date date1 = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(tglMasuk);
		System.out.println("Input tgl dan waktu Keluar Gedung: ");
		String tglKeluar = input.nextLine();
		Date date2 = new SimpleDateFormat("dd/MM/yyy HH.mm").parse(tglKeluar);
		long parkir = 0;
		long selisih = Math.abs(date2.getTime() - date1.getTime());
		System.out.println(date2.getTime());
		long selisihJam = selisih / (60 * 60 * 1000);
		long selisihMenit = selisih / (60*1000) % 60;
		long selisihHari = selisih/ (24*60*60*1000);
		
		if(selisihMenit > 0) {
			selisihJam +=1;
		}
		parkir = selisihHari > 0 ? 15000 * selisihHari : 3000 * selisihJam;
		System.out.println();
		System.out.println("Selisih Hari : " +selisihHari + " Hari");
		System.out.println("Selisih Jam : " +selisihJam+ " Jam");
		System.out.println("Biaya Parkir : " + parkir);

	}

	private static void soal7() {
		System.out.println("Nomor 7");
		System.out.println("Balikin Buku");

	}

	private static void soal8() {
		System.out.println("Ini Nomor 8");
		System.out.println("Kalo Dibalik Sama Kata nya");
		System.out.println("Masukan Kata: ");
		String kata = input.next().toLowerCase();
		char[] CharKata = kata.toCharArray();

		if (kata.equals("bakso") || kata.equals("soup")) {
			System.out.println("Tumpah lah");
			return;
		}

		String temp = "";
		for (int i = CharKata.length - 1; i >= 0; i--) {
			temp += CharKata[i];
		}
		if (temp.equals(kata)) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}

	private static void soal9() {

	}

	private static void soal10() {

	}

}
