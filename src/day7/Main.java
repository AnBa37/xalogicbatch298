package day7;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not Available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal3() {
		System.out.println("Soal Simple Array Sum");

		System.out.println("Masukan Panjang Array: "); //input panjang array yang diinginkannya
		int panjangArray = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Isi Array: "); //tentu isinya
		String isiArray = input.nextLine();

		String[] arrIsi = isiArray.split(" ");

		if (panjangArray != arrIsi.length) { // ini buat kalo ada panjang arraynya gak sesuai dengan isi
			System.out.println("Panjang Tidak Sesuai ");

		}
		int tampung = 0;
		for (int i = 0; i < arrIsi.length; i++) {
			tampung += Integer.parseInt(arrIsi[i]);
		}
		System.out.println(tampung);
	}

	private static void soal4() {
		System.out.println("Diagonal Difference");
		System.out.println("Masukan Nilai: ");
		int nilai = input.nextInt();

		int[][] matriks = new int[nilai][nilai];
		for (int i = 0; i < nilai; i++) {
			for (int j = 0; j < nilai; j++) {
				System.out.println("Nilai elemen Matriks [" + i + "][" + j + "] : ");
				matriks[i][j] = input.nextInt();
			}
		}
		int diagonal = 0;
		for (int i = 0; i < nilai; i++) {
			diagonal += matriks[i][i];
		}

		int diagonal1 = 0;
		for (int j = 0; j < nilai; j++) {
			diagonal1 += matriks[j][(nilai - 1) - j];
		}
		int beda = Math.abs(diagonal - diagonal1);
		System.out.println("Diagonal 1= " + diagonal);
		System.out.println("Diagonal 2= " + diagonal1);
		System.out.println("Beda :" + beda);

	}

	private static void soal5() {
		System.out.println("Plus Minus");

		System.out.println("Masukan Inputan: ");
		int inputan = input.nextInt();

		int[] baris = new int[inputan];
		float positif = 0;
		float negatif = 0;
		float nol = 0;
		for (int i = 0; i < inputan; i++) {
			System.out.println("Masukan Nilai ke - " + (i + 1) + ":");
			baris[i] = input.nextInt();

			if (baris[i] < 0) {
				negatif++;
			}
			if (baris[i] == 0) {
				nol++;
			}
			if (baris[i] > 0) {
				positif++;
			}

		}
		System.out.println(" " + (positif / inputan));
		System.out.println(" " + (negatif / inputan));
		System.out.println(" " + (nol / inputan));

	}

	private static void soal6() {
		System.out.println("StairCase");
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Nilai N: ");
		int n = input.nextInt();
		for (int i = 0; i < n; i++) {// barisnya
			for (int j = 0; j < n; j++) {// kolomnya
				if (i + j >= n) { // buat bikin segitiganya
					System.out.print("#");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

	private static void soal7() {
		System.out.println("Min-Max Sum");

		System.out.println("Masukan Input");
		String n = input.nextLine();

		String[] nStr = n.split(" ");

		
		int min = Integer.MAX_VALUE; // inisiasi buat min
		int max = Integer.MIN_VALUE;// inisiasi buat max

		for (int i = 0; i < nStr.length; i++) {
			int proses = 0;
			for (int j = 0; j < nStr.length; j++) {
				if (i != j) {
					proses += Integer.parseInt(nStr[j]);
				}
			}

			if (max < proses) { // nilai maksnya
				max = proses;
			}
			if (min > proses) { // nilai minimum
				min = proses;
			}
		}
		System.out.print(" " + min);
		System.out.print(" " + max);
	}

	private static void soal8() {
		System.out.println("Birtday Candles");
		System.out.println("Masukan N: ");
		int n = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Arraynya: ");
		String array = input.nextLine();

		String[] StringArray = array.split(" ");

		// nilai max
		int max = 0;
		for (int i = 0; i < StringArray.length; i++) {
			if (max < Integer.parseInt(StringArray[i])) {
				max = Integer.parseInt(StringArray[i]);
			}
		}
		int max1 = 0;
		for (int i = 0; i < StringArray.length; i++) {
			if (max == Integer.parseInt(StringArray[i])) {
				max1++;
			}
		}
		if (n != StringArray.length) { // jika nanti array tidak sesuai
			System.out.println("Panjang Tidak Sesuai");
		}
		System.out.println(max1);

	}

	private static void soal9() {
		System.out.println("A Very Big Sum");

		System.out.println("Masukan Inputan Penjumlahan: ");
		int penjumlahan = input.nextInt();

		long[] baris = new long[penjumlahan];

		long tot = 0;

		for (int i = 0; i < penjumlahan; i++) {
			System.out.println("Masukan Angka ke- " + (i + 1) + ": ");
			baris[i] = input.nextLong();
			tot += baris[i];
		}
		System.out.println("Jadi Total Penjumlahannya adalah =" + tot);
	}

	private static void soal10() {
		System.out.println("Compare the Triplet");
		System.out.println();
		System.out.println("Masukan Arraynya: ");
		String Str1 = input.nextLine(); //untuk baris 1
		String Str2 = input.nextLine(); // untuk baris 2
		
		String[] arrayStr1 = Str1.split(" ");
		String[] arrayStr2 = Str2.split(" ");
		
		int PointA = 0;
		int PointB = 0;
		for(int i=0; i< arrayStr1.length; i++) {
			int A = 0;
			for(int j = 0; j< arrayStr2.length; j++) {
				int B =0;
				
				if(i==j) { 
					A = Integer.parseInt(arrayStr1[i]);
					B = Integer.parseInt(arrayStr2[j]);
					if(A>B) { //untuk perbandingan A sama B
						PointA++;
					} else if(A<B) {
						PointB++;
					}
				}
			}
		}
		if(arrayStr1.length != arrayStr2.length) {
			System.out.println("Panjang Tidak sama, Tolong diinput kembali");
		}
		System.out.println(PointA + " " +PointB);
	}
}
