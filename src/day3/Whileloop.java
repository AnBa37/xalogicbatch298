package day3;

import java.util.Scanner;

public class Whileloop {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Input Angka : ");
		int angka = input.nextInt();
		int i = 0;
		while(i < angka) {
			System.out.println(i+1);
			i++;
		}
		System.out.println("");
		int j=angka;
		while(j>0) {
			System.out.println(j);
			j--;
		}
		input.close();

	}

}
