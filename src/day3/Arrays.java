package day3;

public class Arrays {
	public static void main(String args[]) {
		int[] arraysAngka = new int[5];
		
		String dua = "2";
		//convert String to Int
		int tempAngka = Integer.parseInt(dua);
		
		arraysAngka[0] = 1;
		//memasukan data ke dalam array
		arraysAngka[1]= tempAngka;
		arraysAngka[2] = 3;
		arraysAngka[3] = 4;
		arraysAngka[4] = 5;
		
		//akses nilai di suatu array
		int aksesAngka = arraysAngka[1];
		
		//Array Loop
		for(int i = 0; i< arraysAngka.length; i++) {
			System.out.print(arraysAngka[i] + " ");
		}
		
		System.out.println();
		
		System.out.println("Panjang Array " + arraysAngka.length);
		System.out.println("=========================");
		System.out.println(aksesAngka);
		
		String[] arraysPembelian = new String[5];
		
		int pembelianPertama = 10000;
		int pembelianKedua = 20000;
		//convert dari int ke String
		arraysPembelian[0] = String.valueOf(pembelianPertama);
		arraysPembelian[1] = pembelianKedua + "";
		System.out.println("=========================");
		System.out.println("Nilai Pembelian : " + arraysPembelian[0] + " "+ arraysPembelian[1]);
	}
}
