package day8;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not Available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("=======");
		System.out.println("Soal No1. Camel Case");
		System.out.println("Masukan Kalimat: ");
		String kata = input.nextLine();
		
		// buat jadiin char
		char[] charKata = kata.toCharArray();

		int temp = 0; //variabel tampungan
		for (int i = 0; i < charKata.length; i++) {
			if (Character.isUpperCase(charKata[i] || i == 0)) { 
				temp++;

			}
		}
		System.out.println(temp);

	}

	private static void soal2() {
		System.out.println("=======");
		System.out.println("Soal No2. Strong Password");
		System.out.println("Masukan Panjang Passwordnya: ");
		int Pass2 = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Password: ");
		char[] pass = input.nextLine().toCharArray();

		// Kriteria Password
		char[] numbers = "0123456789".toCharArray();
		char[] lower_case = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		char[] upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		char[] special_char = "!@#$%^&*()-+".toCharArray();

//		int temp = 0;
		if(Pass2 != pass.length) {
			System.out.println("Jumlah karakter yang anda masukan tidak sesuai");
			return;
		}
		if(pass.length < 6) {
			System.out.println(6 - pass.length + " digit character, masukan setidaknya 6 digit character");
			return;
		}
		
		int tempNum = 0;
		int templow = 0;
		int tempup = 0;
		int tempspes = 0;
		
		for(int i = 0; i<pass.length;i++) {
			for(int j= 0; j< numbers.length; j++) {
				if(pass[i] == numbers[j]) {
					tempNum++;
				}
			}
			for(int k=0; k< lower_case.length; k++) {
				if(pass[i] == lower_case[k]) {
					templow++;
				}
			}
			for(int l = 0; l< upper_case.length; l++) {
				if(pass[i] == upper_case[l]) {
					tempup++;
				}
			}
			for(int m = 0; m < special_char.length; m++) {
				if(pass[i] == special_char[m]) {
					tempspes++;
				}
			}
		}
		if(tempNum == 0 || templow == 0 || tempup == 0 || tempspes == 0) {
			System.out.println("Maaf, Password anda lemah");
		}
		if(tempNum == 0) {
			System.out.println("Password Anda Setidaknya membutuhkan 1 digit nomor");
		}
		if(templow == 0) {
			System.out.println("Password Anda Setidaknya membutuhkan 1 lowercase English character");
		}
		if(tempup == 0) {
			System.out.println("Password Anda Setidaknya membutuhkan 1 Uppercase English character");
		}
		if(tempspes == 0) {
			System.out.println("Password Anda Setidaknya membutuhkan 1 special Character seperti !@#$%^&*()-+");
		}
		else {
			System.out.println(" Password anda Telah Masuk");
		}
	}

	private static void soal3() {
		System.out.println("=======");
		System.out.println("Soal No3. Caesar Cipher");
		String original_alphabet = "abcdefghijklmnopqrstuvwxyz";
		System.out.println("Masukan Panjang Array: ");
		int panjangArr = input.nextInt();
		input.nextLine();
		System.out.println("Masukan Kalimat: ");
		String pesan = input.next().toLowerCase();
		
		System.out.println("Berapa kali ingin di Shift: ");
		int geser = input.nextInt();
		String cipherText = "";
		for(int i =0 ; i< pesan.length(); i++) {
			int posisi = original_alphabet.indexOf(pesan.charAt(i));
			int geser1 = (geser+posisi) % 26;
			char replace = original_alphabet.charAt(geser1);
			cipherText += replace;
		}
		System.out.println("Pesannya yang Telah Ter Enkripsi adalah " + cipherText);
	}

	private static void soal4() {
		System.out.println("=======");
		System.out.println("Soal No4. Mars Exploration");
		System.out.println("Masukan Pesan SOS: ");
		String sos = input.nextLine();

		char[] sos1Char = sos.toLowerCase().toCharArray();
		int temp = 0;
		for (int i = 0; i < sos1Char.length; i++) {
			if (i % 3 == 0 && sos1Char[i] == 's') {
				continue;
			} else if (i % 3 == 1 && sos1Char[i] == 'o') {
				continue;
			} else if (i % 3 == 2 && sos1Char[i] == 's') {
				continue;
			} else {
				temp++;
			}
		}

		System.out.println(temp);
	}

	private static void soal5() {
		System.out.println("=======");
		System.out.println("Soal No5. HackerRank in a String!");
		System.out.println("Masukan total Kata: ");
		int totKata = input.nextInt();
		int n = 0;
		String[] StringHasil = new String[totKata];
		
		while(n<totKata) {
			System.out.println("Masukan Kata: ");
			String kata = input.next();
			char[] charKata = kata.toLowerCase().toCharArray();
			int[] intHackerRank = new int[7];
			
			int temp = 0;
			
			for(int i=0; i<charKata.length;i++) {
				if(charKata[i] == 'h') {
					intHackerRank[0] += 1;
				}else if(charKata[i] == 'a' && (intHackerRank[0]>=1 || intHackerRank[5]>=2)) {
					intHackerRank[1] +=1;
				} else if(charKata[i]== 'c' && intHackerRank[1]>=1) {
					intHackerRank[2] +=1;
				} else if(charKata[i]=='k' && intHackerRank[2]>=1) {
					intHackerRank[3] +=1;
				} else if(charKata[i]=='e' && intHackerRank[3]>=1) {
					intHackerRank[4] +=1;
				} else if(charKata[i]=='r' && (intHackerRank[4]>=1 || intHackerRank[5]>=1)) {
					intHackerRank[5] +=1;
				} else if(charKata[i]=='n' && intHackerRank[1] >=1) {
					intHackerRank[6] +=1;
				}
			}
			
			for(int i=0; i<intHackerRank.length;i++) {
				if(intHackerRank[i]==0) {
					temp = 0;
					break;
				} else {
					temp = 1;
				}
			}
			if(intHackerRank[5]<2) {
				temp = 0;
			}
			if(temp == 1) {
				StringHasil[n] = "YES";
			}else {
				StringHasil[n] = "NO";
			}
			n++;
		}
		for(int i =0; i< StringHasil.length;i++) {
			System.out.println(StringHasil[i]);
		}
		
	}

	private static void soal6() {
		System.out.println("=======");
		System.out.println("Soal No6.Pangrams");
		System.out.println("Masukan Kalimat: ");
		char[] kalimat = input.nextLine().toLowerCase().toCharArray();
		char[] ori_alfa = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		
		int temp = 0;
		for(int i = 0; i< ori_alfa.length; i++) {
			for(int j = 0; j< kalimat.length; j++) {
				if(kalimat[j] == ori_alfa[i]) {
					temp ++;
					break;
				}
			}
		}
		if(temp == 26) {
			System.out.println("Pangrams");
		} else {
			System.out.println("Not Pangrams");
		}

	}

	private static void soal7() {
		System.out.println("=======");
		System.out.println("Soal No7. Separate The Numbers");
		String Angka = "12";
		char[] AngkaChar = Angka.toCharArray();
		int JumlahAngka = 1;
		String Hasil = "YES";
		for(int i=0; i< AngkaChar.length; i++) {
			if(i>0) {
				if((char) AngkaChar[i] - (char) AngkaChar[i-1] != 1){
					Hasil = "NO";
					break;
				}
			}
		}
		System.out.println(Hasil);
	}

	private static void soal8() {
		System.out.println("=======");
		System.out.println("Soal No8. Gemstones");
		System.out.println("Masukan Banyak Kata: ");
		int banyakKata = input.nextInt();
		input.nextLine();
		
		System.out.println("Masukan Kata: ");
		String inputan = input.nextLine();
		String[] inputanString = inputan.split(" ");
		char[] inputanChar = inputan.toCharArray();
		char[] alfabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
				
	}

	private static void soal9() {
		System.out.println("=======");
		System.out.println("Soal No9. Making Anagram");
		System.out.println("Masukan 2 Kata: ");
		String kata1 = input.next();
		String kata2 = input.next();
		
		char[] charKata1 = kata1.toCharArray();
		char[] charKata2 = kata2.toCharArray();
		
		int tampung = 0;
		for(int i = 0; i<charKata1.length; i++) {
			for(int j = 0; j <charKata2.length; j++) {
				if(charKata1[i] == charKata2[j]) {
					tampung++;
				}
			}
		}
		tampung = (charKata1.length + charKata2.length) - (tampung*2);
		System.out.println(tampung);
	}

	private static void soal10() {
		System.out.println("=======");
		System.out.println("Soal No10. Two String");
		System.out.println("Masukan Kata: ");
		int kata = input.nextInt();
		input.nextLine();
		String[][] StringKata = new String[kata][2];
		
		for(int i=0; i< kata; i++) {
			for(int j=0; j < 2; j++) {
				System.out.println("Silahkan Masukan Data ke -" + (i+1) + "dan data Ke -" + (j+1) + " : ");
				StringKata[i][j] = input.nextLine().toLowerCase();
			}
		}
		for(int i=0; i< kata; i++) {
			int temp =0;
			for(int j=0; j <StringKata.length;j++) {
				for(int l=0; l < StringKata[i][1].length(); l++) {
					if(StringKata[0][0].charAt(j) == StringKata[0][1].charAt(l)) {
						temp++;
						break;
					}
				}
			}
			if(temp>0) {
				System.out.println("YES");
			}else {
				System.out.println("NO");
			}
		}
	}
}
