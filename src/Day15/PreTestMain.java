package Day15;

import java.util.Scanner;

public class PreTestMain {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not Available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {
		System.out.println("Soal No.1");
		System.out.println("Angka Ganjil Genap");
		System.out.println("Masukan Bilangan: ");
		int bilangan = input.nextInt();
//		int[] arrN = new int[bilangan];
//		String temp = " ";
		int temp = 0;
//		int temp1 =0;
		for (int i = 1; i < bilangan; i++) {
			if (i % 2 != 0) {
				temp = i + 2;
				System.out.print(i + " ");
				i++;

			}
			if (i % 2 == 0) {
				temp = i + 2;
				System.out.print(i + " ");
				i++;

			}
			if (i < 0) {
				System.out.println("Maaf Angka Bersifat Negatif");
			}
		}
//

	}

	private static void soal2() {
		System.out.println("Urutkan Sesuai Kalimat");
		System.out.println("Masukan Kalimat: ");
		char[] kalimat = input.nextLine().toLowerCase().trim().toCharArray();
		String konsonan = "";
		String vokal = "";

		for (int i = 0; i < kalimat.length; i++) {
			if (kalimat[i] == 'a' || kalimat[i] == 'i' || kalimat[i] == 'u' || kalimat[i] == 'e' || kalimat[i] == 'o') {
				vokal += kalimat[i];

			} else {
				konsonan += kalimat[i];
			}

		}
		char temp = 0;
		char[] konsonan1 = konsonan.toLowerCase().toCharArray();
		char[] vokal1 = vokal.toLowerCase().toCharArray();
		for (int i = 0; i < kalimat.length; i++) {
			for (int j = 0; j < vokal1.length; j++) {
				if ((int) vokal1[i] <= (int)vokal1[j]) {
					temp = kalimat[i];
					kalimat[i] = kalimat[j];
					kalimat[j] = temp;
				}
			}
		}
		for (int i = 0; i < konsonan1.length; i++) {
			for (int j = 0; j < konsonan1.length; j++) {
				temp = konsonan1[i];
				konsonan1[i] = konsonan1[j];
				konsonan1[j] = temp;
			}
		}
		System.out.println("Vokal: " +String.valueOf(vokal1));
		System.out.println("Konsonan: " +String.valueOf(konsonan1));
//		System.out.println(kalimat);
//		tempChar = 0;
//		for (int i = 0; i < kalimat.length; i++) {
//			if (kalimat[i] != 32) {
//				if (tempChar != kalimat[i] && i > 1) {
//					temp = " - ";
//				}
//				tempChar = kalimat[i];
//				temp += kalimat[i];
//			}
//		}
//		System.out.println(temp);
	}

	private static void soal3() {

	}

	private static void soal4() {
		System.out.println("Rute Tempuh");
		System.out.println("Masukan Rute: ");
		String rute = input.nextLine();
		String[] ruteString = rute.split(" ");
		int[] ruteInt = new int[ruteString.length];
		double hasil = 0;
		double bensin = 2.5;
		double[] jarak = { 2, 0.5, 1.5, 2.5 };

		for (int i = 0; i < ruteString.length; i++) {
			hasil = hasil + jarak[Integer.parseInt(ruteString[i]) - 1];
			if (i != ruteString.length - 1) {
				System.out.print(jarak[Integer.parseInt(ruteString[i]) - 1] + " + ");
			} else {
				System.out.print(jarak[Integer.parseInt(ruteString[i]) - 1] + " = " + hasil + " KM ");
			}
		}
		System.out.println();
		bensin = hasil / bensin;
		bensin = Math.ceil(bensin);
		String tempString = "Bensin yang dikonsumsi = " + bensin + " liter";
		System.out.println(tempString.replaceAll("0", " "));

	}

	private static void soal5() {
		System.out.println("Jajanan Publik");

		int[] pelanggan = new int[5];
		System.out.println("Masukan Laki-Laki Dewasa: ");
		pelanggan[0] = input.nextInt() * 2;
		System.out.println("Masukan Perempuan Dewasa: ");
		pelanggan[1] = input.nextInt() * 1;
		System.out.println("Masukan Remaja: ");
		pelanggan[2] = input.nextInt() * 1;
		System.out.println("Masukan Anak-Anak: ");
		pelanggan[3] = input.nextInt() * 1 / 2;
		System.out.println("Masukan Balita: ");
		pelanggan[4] = input.nextInt() * 1;

		int makanan = 0;

		for (int i = 0; i < pelanggan.length; i++) {
			makanan += pelanggan[i];
		}
		if (makanan % 2 == 1 && pelanggan.length > 5) {
			makanan -= pelanggan[1] + 1;
		}
		System.out.println(makanan + " Porsi");
	}

	private static void soal6() {
		
	}

	private static void soal7() {
		System.out.println("Permainan gambaran gag");
		System.out.println("Masukan Tawaran: ");
		int tawaran = input.nextInt();
		
		String pil = "Y";
		while(pil.toUpperCase().equals("Y")) {
			System.out.println("Masukan Jumlah Kartu Atau Gambaran: ");
			int jumlah = input.nextInt();
			System.out.println("Kotak (A/B): ");
			String kotak = input.next();
			
			int random = (int) (Math.random()*9);
			String hasil = "";
			if(random>5) {
				if(kotak.equals("A")) {
					tawaran = tawaran + jumlah;
					hasil = "You Win!";
				}
				if(kotak.equals("B")) {
					tawaran = tawaran - jumlah;
					hasil = "You Lose!";
				}
			}
			if(random == 5) {
				if(kotak.equals("B") || kotak.equals("A")) {
					hasil = "Draw";
				}
			}
			if(random < 5) {
				if(kotak.equals("B")) {
					tawaran = tawaran + jumlah;
					hasil = "You Win!";
				}
				if(kotak.equals("A"))
					tawaran = tawaran - jumlah;
					hasil = "You Lose!";
			}
		}

	}

	private static void soal8() {

	}

	private static void soal9() {

	}

	private static void soal10() {

	}

}
