package day14;

import java.util.Scanner;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ContohSoal {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of case : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
//				case 3:
//					soal3();
//					break;
//				case 4:
//					soal4();
//					break;
//				case 5:
//					soal5();
//					break;
//				case 6:
//					soal6();
//					break;
//				case 7:
//					soal7();
//					break;
//				case 8:
//					soal8();
//					break;
//				case 9:
//					soal9();
//					break;
//				case 10:
//					soal10();
//					break;
				default:
					System.out.println("Case is not Available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() throws ParseException {
//		System.out.println("Nomor 1");
//		System.out.println("Masukan Nilai X: ");
//		int x = input.nextInt();
//		System.out.println("Masukan Nilai Y: ");
//		int y = input.nextInt();
//		input.nextLine();
//		System.out.println("Masukan Nilai Z: ");
//		String z = input.nextLine();
//
//		Date date1 = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(z);
		System.out.println("Masukkan libur Mary setiap x hari: ");
		int x = input.nextInt() + 1;// 4;//
		System.out.println("Masukkan libur Susan setiap y hari: ");
		int y = input.nextInt() + 1;// 3;// input.nextInt() + 1;
		input.nextLine();
		System.out.println("Masukkan tanggal terakhir libur bersama: ");
		String z = input.nextLine(); // input.nextLine(); // "25 February 2020";

		String output = "";

		// handling
		boolean apaInputanSesuai = true;
		boolean apaAdaKarakterLain = false;
		char[] waktu = z.toCharArray();
		for (int i = 0; i < waktu.length; i++) {
			// periksa apa ada karakter lain selain cth: 25 February 2020
			if ((waktu[i] >= 48 && waktu[i] <= 57) || waktu[i] == 32 || (waktu[i] >= 65 && waktu[i] <= 90)
					|| (waktu[i] >= 97 && waktu[i] <= 122)) {
				apaAdaKarakterLain = false;
			} else {
				apaAdaKarakterLain = true;
				break;
			}

			// periksa apa inputan sesuai
			if (i <= 1) {
				if (waktu[i] >= 48 && waktu[i] <= 57) {
					apaInputanSesuai = true;
				} else {
					apaInputanSesuai = false;
					break;
				}
			} else if (i > waktu.length - 5) {
				if ((waktu[i] >= 48 && waktu[i] <= 57) || waktu[i] == 32) {
					apaInputanSesuai = true;
				} else {
					apaInputanSesuai = false;
					break;
				}

				output += String.valueOf(waktu[i]);
			}
		}

		if (apaAdaKarakterLain || !apaInputanSesuai) {
			System.out.println("input tidak sesuai");
			return;
		}

		// ubah bulan dari bahasa indo ke eng
		String[] zSplit = z.split(" ");
		z = "";
		int intBulan = 0;
		for (int i = 0; i < zSplit.length; i++) {
			if (i == 1) {
				String tempBulan = zSplit[i].toLowerCase();
				if (tempBulan.contains("jan")) {
					tempBulan = "January";
					intBulan = 1;
				} else if (tempBulan.contains("feb")) {
					tempBulan = "February";
					intBulan = 2;
				} else if (tempBulan.contains("mar")) {
					tempBulan = "March";
					intBulan = 3;
				} else if (tempBulan.contains("apr")) {
					tempBulan = "April";
					intBulan = 4;
				} else if (tempBulan.contains("mei")) {
					tempBulan = "May";
					intBulan = 5;
				} else if (tempBulan.contains("jun")) {
					tempBulan = "June";
					intBulan = 6;
				} else if (tempBulan.contains("jul")) {
					tempBulan = "July";
					intBulan = 7;
				} else if (tempBulan.contains("agu")) {
					tempBulan = "August";
					intBulan = 8;
				} else if (tempBulan.contains("sep")) {
					tempBulan = "September";
					intBulan = 9;
				} else if (tempBulan.contains("okt")) {
					tempBulan = "October";
					intBulan = 10;
				} else if (tempBulan.contains("nov")) {
					tempBulan = "November";
					intBulan = 11;
				} else if (tempBulan.contains("des")) {
					tempBulan = "December";
					intBulan = 12;
				}
				z += " " + tempBulan + " ";
			} else {
				z += zSplit[i];
			}
		}

		// handling kabisat
		zSplit = z.split(" ");
		boolean apaTanggalSesuai = true;
		int hari = Integer.parseInt(zSplit[0]);
		int bulan = intBulan;
		int tahun = Integer.parseInt(zSplit[0]);
		if (bulan > 0 && bulan <= 7) {
			if (bulan == 2 && hari <= 28) {
				apaTanggalSesuai = true;
			} else if (bulan == 2 && hari <= 29 && tahunKabisat(tahun)) {
				apaTanggalSesuai = true;
			} else if (bulan % 2 == 1 && hari <= 31) {
				apaTanggalSesuai = true;
			} else if (bulan % 2 == 0 && hari <= 30 && bulan != 2) {
				apaTanggalSesuai = true;
			} else {
				apaTanggalSesuai = false;
			}
		} else if (bulan > 7 && bulan <= 12) {
			if (bulan % 2 == 0 && hari <= 31) {
				apaTanggalSesuai = true;
			} else if (bulan % 2 == 1 && hari <= 30) {
				apaTanggalSesuai = true;
			} else {
				apaTanggalSesuai = false;
			}
		}

//		System.out.println(apaTanggalSesuai + " " + bulan + " " + tahunKabisat(tahun));
		if (!apaTanggalSesuai) {
			System.out.println("Tanggal tidak sesuai, silahkan periksa kembali");
			return;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
		Date dateZ = dateFormat.parse(z);
		System.out.println(dateZ);

		long hariLiburBersama = 0;
		boolean apaLiburBersama = false;
		while (!apaLiburBersama) {
			if (hariLiburBersama % x == 0 && hariLiburBersama % y == 0 && hariLiburBersama > 0) {
				apaLiburBersama = true;
				continue;
			}
			hariLiburBersama++;
		}

		output = dateFormat.format(dateZ.getTime() + (hariLiburBersama * 1000 * 3600 * 24));

		System.out.println(output);

	}


	

	private static void soal2() {
		System.out.println("Masukkan kalimat: ");
		char[] inputChar = input.nextLine().toLowerCase().trim().toCharArray();
		String output = "";

		// sorting
		char tempChar = 0;
		for (int i = 0; i < inputChar.length; i++) {
			for (int j = 0; j < inputChar.length; j++) {
				if (inputChar[i] < inputChar[j]) {
					tempChar = inputChar[i];
					inputChar[i] = inputChar[j];
					inputChar[j] = tempChar;
				}
			}
		}
		System.out.println(inputChar);

		tempChar = 0;
		for (int i = 0; i < inputChar.length; i++) {
			if(inputChar[i] != 32) {
				if (tempChar != inputChar[i] && i > 1) {
					output += " - ";
				}
				tempChar = inputChar[i];
				output += inputChar[i];
			}

		}

		System.out.println(output);
	}
	


//	System.out.println("Masukan Kalimat: ");
//	String kalimat = input.nextLine();
//	
//	char[] kalimatChar = kalimat.toLowerCase().trim().toCharArray();
//	String konsonan = "";
//	String vokal = "";
//	
////	for(int i =0; i < kalimatChar.length; i++) {
////		if (kalimatChar[i] == 'a' || kalimatChar[i] == 'i' || kalimatChar[i] == 'u' || kalimatChar[i] == 'e'
////				|| kalimatChar[i] == 'o') {
////			vokal += kalimatChar[i];
////		} else {
////			konsonan += kalimatChar[i];
////		}
////	}
////	char[] konsonan1 = konsonan.toLowerCase().toCharArray();
////	char[] vokal1 = vokal.toLowerCase().toCharArray();
////	
////	char temp = 0;
////	for (int i = 0; i < vokal1.length; i++) {
////		for (int j = 0; j < vokal1.length; j++) {
////			if ((int) vokal1[i] <= (int) vokal1[j]) {
////				temp = vokal1[i];
////				vokal1[i] = vokal1[j];
////				vokal1[j] = temp;
////			}
////		}
////	}
//	char temp = " ";
//	for(int i = 0; i < kalimat.length; i++) {
//		
//	}
//	String output = "";
//	for(int i= 0; i < kalimatChar.length;i++) {
//		if(kalimatChar[i] != 32) {
//			if(temp != kalimatChar[i] && i > 1) {
//				output += "-";
//			}
//			temp = kalimatChar[i];
//			output += kalimatChar[i];
//		}
//	}
//	System.out.println(output);
//}
	

}