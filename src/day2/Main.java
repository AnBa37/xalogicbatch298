package day2;

import java.util.Scanner;

public class Main {
	public static void main(String args[]) {
		Scanner input = new Scanner (System.in);
		System.out.println("Input Hadiah (Pilih Nomor 1-5) : ");
		
		int nomorHadiah = input.nextInt();
		
		switch(nomorHadiah) {
			case 1:
				System.out.println("Selamat Anda Mendapatkan Mobil");
				break;
			case 2:
				System.out.println("Selamat Anda Mendapatkan Motor");
				break;
			case 3:
				System.out.println("Selamat Anda Mendapatkan Villa di Lembang");
				break;
			case 4:
				System.out.println("Selamat Anda Mendapatkan tiket ke Jepang");
				break;
			case 5:
				System.out.println("Selamat Anda Mendapatkan Tiket ke Rumah");
				break;
				default:
					System.out.println("Nomor yang anda Pilih Tidak Sesuai");
					break;
		}
	}

}
