package day2;

import java.util.Scanner;

public class Nomor1 {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		System.out.println("Jumlah Pulsa Yang akan Dibeli : Rp.");
		int pulsa = input.nextInt();
		
		//Pulsa 10k = 80 point
		//pulsa 25k = 200 point
		//pulsa 50k = 400 point
		//pulsa 100k = 800 point
		 
		if(pulsa >= 10000 && pulsa <=24999) {
			System.out.println( "Anda Telah Membeli Pulsa Sebesar Rp. " + pulsa + " dan Berhak mendapatkan 80 point");
		} else if (pulsa >=25000 && pulsa <= 49999) {
			System.out.println("Anda Telah Membeli Pulsa Sebesar Rp. " + pulsa + " dan Berhak mendapatkan 200 point");
		} else if (pulsa >= 50000 && pulsa <=99999) {
			System.out.println("Anda Telah Membeli Pulsa Sebesar Rp. " + pulsa + " dan Berhak mendapatkan 400 point");
		} else if(pulsa >= 100000) {
			System.out.println("Anda Telah Membeli Pulsa Sebesar Rp. " + pulsa + " dan Berhak mendapatkan 800 point");
		} else {
			System.out.println("Anda Mendapatkan " + pulsa + " dan mendapatkan 0 Point");
		}
	}

}
